FROM c3h3/rstudio-java:latest

MAINTAINER Chia-Chi Chang <c3h3.tw@gmail.com>

RUN mkdir /demo
ADD package_installer.R /demo/package_installer.R
RUN cd /demo && Rscript package_installer.R


